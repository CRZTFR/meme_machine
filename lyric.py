"""
Reads the song lyric csv file and returns a random row
"""
import pandas as pd
import random

# Read csv
csvfile = pd.read_csv('songdata.csv')

# Select random line
sample = csvfile.sample()

# Extract artist and lyrics
artist = sample.iloc[0]['artist']
lyric = sample.iloc[0]['text']
numlines = random.randint(1,4)
lyrics = lyric.splitlines(True)
verse = ''
for line in range(numlines):
    verse+=lyrics[line]


# Print
print(verse)
print(artist)
print('on memes')
